module App

open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful

let webPart =
    choose [
        path "/" >=> OK "Hello world"
        ]
