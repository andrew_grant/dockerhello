FROM microsoft/aspnetcore-build:2.0.7-2.1.105 AS build-env
COPY HelloServer /HelloServer
COPY HelloTests /HelloTests

WORKDIR /HelloTests

RUN dotnet test

WORKDIR /HelloServer

RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/aspnetcore:2.0.7
WORKDIR /app
COPY --from=build-env /HelloServer/out .
EXPOSE 5000/tcp
ENV ASPNETCORE_URLS http://*:5000
ENTRYPOINT ["dotnet", "HelloServer.dll"]
